CREATE DATABASE CARCONNECT
--CUSTOMER_TABLE
CREATE TABLE CUSTOMER(CUSTOMER_ID INT PRIMARY KEY,
FIRST_NAME VARCHAR(255),
LASTNAME VARCHAR(255),
EMAIL VARCHAR(255),
PHONE_NUMBER VARCHAR(20),
CUSTOMER_ADDRESS VARCHAR(255),
USERNAME VARCHAR(255) UNIQUE,
PASSWORD VARCHAR(255),
REGISTRATION_DATE DATE)

-- Inserting  data into the Customer table
INSERT INTO Customer (CUSTOMER_ID, FIRST_NAME, LASTNAME, Email, PHONE_NUMBER, CUSTOMER_ADDRESS, USERNAME, PASSWORD, REGISTRATION_DATE)
VALUES
(1, 'BHANU', 'PRAKASH', 'bhanu@example.com', '1234567890', '123 prasad nagar, vizag', 'bhanuprakash', 'password123', '2024-01-01'),
(2, 'DHARANI', 'NAIDU', 'vinay@example.com', '9876543210', '456 pradeep nagar, vizag', 'dharaninaidu', 'dharani123', '2024-02-15'),
(3, 'VINAY', 'KUMAR', 'kumar@example.com', '5551234567', '789 ganesh nagar, vizag', 'vinaykumar', 'securepassword', '2024-03-20'),
(4, 'PUSHPA', 'RANI', 'pushpa@example.com', '3339876543', '321 gandhi nagar, vizag', 'pushparani', 'password1234', '2024-04-10'),
(5, 'ESWAR', 'RAO', 'eswar@example.com', '1112223333', '654 durga nagar, vizag', 'eswarrao', 'password!@#', '2024-05-05')

--VEHICLE TABLE
CREATE TABLE VEHICLE (
    VEHICLE_ID INT PRIMARY KEY,
    Model VARCHAR(255),
    Make VARCHAR(255),
    Year INT,
    Color VARCHAR(50),
    RegistrationNumber VARCHAR(20) UNIQUE,
    Availability TINYINT,
    DailyRate DECIMAL(10, 2)
)
-- Inserting  data into the Vehicle table
INSERT INTO VEHICLE(VEHICLE_ID, Model, Make, Year, Color, RegistrationNumber, Availability, DailyRate)
VALUES
(1, 'Camry', 'Toyota', 2019, 'Silver', 'ABC123', 1, 1500.00),
(2, 'Accord', 'Honda', 2020, 'Black', 'XYZ456', 1, 1000.00),
(3, 'Civic', 'Honda', 2018, 'Red', 'DEF789', 0, 1100.00),
(4, 'Mustang', 'Ford', 2021, 'Blue', 'GHI012', 1, 2000.00),
(5, 'Corolla', 'Toyota', 2017, 'White', 'JKL345', 1, 1500.00)

-- Reservation Table
CREATE TABLE RESERVATION(
    ReservationID INT PRIMARY KEY,
    CUSTOMER_ID INT,
    VEHICLE_ID INT,
    StartDate DATETIME,
    EndDate DATETIME,
    TotalCost DECIMAL(10, 2),
    Status VARCHAR(50),
    FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMER(CUSTOMER_ID),
    FOREIGN KEY (VEHICLE_ID) REFERENCES Vehicle(VEHICLE_ID)
)
-- Inserting data into the Reservation table
INSERT INTO Reservation(ReservationID, CUSTOMER_ID, VEHICLE_ID, StartDate, EndDate, TotalCost, Status)
VALUES
(1, 1, 1, '2023-01-10', '2023-01-15', 7500.00, 'confirmed'),
(2, 2, 3, '2023-02-20', '2023-02-25', 5000.00, 'pending'),
(3, 3, 2, '2023-03-15', '2023-03-20', 5500.00, 'confirmed'),
(4, 4, 5, '2023-04-05', '2023-04-10', 10000.00, 'completed'),
(5, 5, 4, '2023-05-10', '2023-05-15', 7500.00, 'confirmed')
EXEC sp_rename Reservation , RESERVATION
-- Admin Table
CREATE TABLE ADMIN (
    AdminID INT PRIMARY KEY,
    FirstName VARCHAR(255),
    LastName VARCHAR(255),
    Email VARCHAR(255),
    PhoneNumber VARCHAR(20),
    Username VARCHAR(50) UNIQUE,
    Password VARCHAR(255),
    Role VARCHAR(50),
    JoinDate DATE
)
-- Inserting data into the Admin table
INSERT INTO ADMIN(ADMIN_ID, FirstName, LastName, Email, PhoneNumber, Username, Password, Role, JoinDate)
VALUES
(1, 'VIJAY', 'PRAKASH', 'vijay@example.com', '9998887777', 'vijayp', 'adminpass', 'super admin', '2023-12-01'),
(2, 'SONU', 'SOODH', 'sonu@example.com', '8887776666', 'sonus', 'password', 'manager', '2023-01-15')
EXEC sp_rename Admin,ADMIN
EXEC sp_rename 'ADMIN.AdminID','ADMIN_ID','COLUMN'
SELECT *FROM CUSTOMER
SELECT *FROM VEHICLE
SELECT *FROM RESERVATION
SELECT *FROM ADMIN